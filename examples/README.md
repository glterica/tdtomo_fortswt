# README #

This folder contains two example datasets that can be used to test the correctness of your compiled programs.

Each example directory is structured as follows:

```
<example>
|-- data
    |-- otimes.dat
    |-- sources.dat
    |-- receivers.dat
|-- inversion
    |-- forward.in
    |-- mksamples.in
    |-- procsamples.in
|-- results
    |-- plots
        |-- ...
    |-- plotting.sh
|-- <synthetic_model>.vtx
```

where:

+ ```data``` contains data files for the inversion
+ ```inversion``` contains the parameter files for inversion and processing
+ ```results/plots``` contains plots from the TSWT inversion results 
+ ```plotting.sh``` contains the plotting parameters used to obtain the plots in ```results/plots```
+ ```<synthetic_model>.vtx``` is the synthetic model used to generate the dataset

The inversion for each example was carried out by running the following:

```
$ mpiexec -np 4 mksamples 1
$ mpiexec -np 4 mksamples 2
$ mpiexec -np 4 mksamples 3
$ mpiexec -np 4 mksamples 4
$ mpiexec -np 4 mksamples 5
$ mpiexec -np 4 mksamples 6
$ ./procsamples
```

where ```mksamples``` and ```procsamples``` were compiled using the GNU compilers.

***

#### Note that the results from these example inversions are only meant to be used to check that your installation is running correcly, hence they were obtained by running *very short* Markov chains (only 6000 samples!) --> the results are not particularly good!! ####

#### When the code is used for real studies, longer Markov chains (order 10^6 samples) should be run in order to move past the burn-in period, reach convergence, and ensure that a large enough ensemble of samples is obtained. ####
