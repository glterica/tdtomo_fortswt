######################################################################
# Leave the following line as it is
[ALL]
######################################################################

# DEFINE A NUMBER OF DIRECTORIES

# - directory containing files mksamples.in, procsamples.in,
#   forward_file, sources_file, receivers_file, data_file
inversion_directory=~/erica/TDTOMO/ForTSWT/bin_square

# - directory containing outputs from procsamples
file_directory=~/erica/TDTOMO/ForTSWT/bin_square

# - directory containing forward modelling and interpolation codes
forward_directory=~/erica/TDTOMO/ForTSWT/bin_square
gmtplot_directory=${forward_directory}/gmtplot

# - directory for output plots
plot_directory=~/erica/TDTOMO/ForTSWT/examples/square_2d/results/plots

######################################################################

# DEFINE A NUMBER OF PROGRAMS

# - programs for map interpolation
map_interpolation=tslicess
voro_interpolation=voroslice
voro2grid=vorogrid

# - program for calculation of the posterior
get_posterior=value_2D_post

######################################################################

# WHAT TO PLOT? ("yes" will plot)

# - average, median, maximum, harmonic mean and root-mean-square
plot_maps_1=yes

# - standard deviation, entropy, node density, skewness and kurtosis
plot_maps_2=yes

# - initial and final Voronoi models (one per chain)
plot_voronoi=yes

# - residuals
plot_residuals=yes

# - posterior on number of cells
plot_post_ncell=yes

# - posterior on noise parameters
plot_post_noise=yes

# - posterior on velocities
plot_post_value=yes

# - number of cells and misfit vs. iteration
plot_chain_evol=yes

######################################################################

# GENERAL PARAMETERS FOR GMT SETUP

# - prefix for GMT commands
gmt_prefix=gmt

# - paper size (in GMT format for PS_MEDIA)
paper_size=Custom_30cx30c

# - thickness of the plot frame
frame_thickness=thick

# - position (in cm) of bottom left corner of maps/plots
x_corner=4
y_corner=3.4

# - format for output plot files (in GMT format)
plot_format="-Tg -E200"

######################################################################

# COLOR PALETTE PARAMETERS

# - location of color palette (.cpt) files
cpt_directory=~/erica/TDTOMO/ForTSWT/source/cptfiles

# - color palette, options, ticks (in GMT format) and colorbar label
#   for average, median, maximum, harmonic mean and root-mean-square
map_cpt=dkbluered.cpt
map_cpt_options="-I -Z" # see -B flag in GMT commands
map_ticks=a1f0.2 # see -B flag in GMT commands
map_label="group velocity (km s@+-1@+)"

# - color palette, options and ticks (in GMT format) for standard 
#   deviation and entropy
std_cpt=jet.cpt
std_cpt_options="-Z" # see -B flag in GMT commands
stdev_ticks=a0.2f0.1 # see -B flag in GMT commands
entropy_ticks=a1f0.2 # see -B flag in GMT commands

# - color palette and ticks (in GMT format) for node density
nod_cpt=GMT_hot.cpt
nod_cpt_options="-Z -I" # see -B flag in GMT commands
nod_ticks=a0.01f0.002 # see -B flag in GMT commands

# - color palette, options, ticks (in GMT format) and maximum value
#   of colorbar for skewness
skew_cpt=dkpurplegreen.cpt
skew_cpt_options="-Z"
max_abs_skew=10
skew_ticks=a1f0.5 # see -B flag in GMT commands

# - color palette, options, ticks (in GMT format) and maximum value
#   of colorbar for kurtosis
kurt_cpt=dkpurplegreen.cpt
kurt_cpt_options="-I"
kurt_ticks=a0.2+0.1 # see -B flag in GMT commands
kurt_sign_only=yes
zero_lim_kurt=0.1

######################################################################

# SETUP FOR MAPS

# - width and height of maps
map_width=3
map_height=0

# - central meridian and standard parallel for geographical projections
longitude0=357
latitude0=54.5

# - thickness of coastlines for geographical projections (in GMT 
#   format)
coastline_thickness=-W0.2

# - x and y ticks (see -B flag in GMT commands)
map_x_ticks=a20f10
map_y_ticks=a20f10

# - x and y labels for maps (see -B flag in GMT commands)
map_x_label="x (km)"
map_y_label="y (km)"

# - position and height (in cm) of the color bar
x_cpt=5.5 #`echo ${x_corner} ${map_width} | awk '{print $1+$2/2}'`
y_cpt=2.4
cpt_height=0.2h

# - interpolation factor for maps and voronoi models
lat_y_dicing_map=5
long_x_dicing_map=5
lat_y_dicing_voro=10
long_x_dicing_voro=10

# - plot sources and receivers? "yes" will plot them
plot_sources=yes
source_symbol="-Sa0.1 -Gwhite -W0.3" # symbol in GMT format
plot_receivers=yes
receiver_symbol="-St0.1 -Gwhite -W0.3" # symbol in GMT format

# - plot raypaths? "yes" will plot them
plot_rays=yes
ray_line="-W0.2"

######################################################################

# SETUP FOR XY PLOTS

# - width and height of plots
xy_width=3
xy_height=3

# - x and y ticks for posterior on number of cells (in GMT format)
#   (if set to "", ticks will be assigned automatically)
ncell_x_ticks=a50f10
ncell_y_ticks=a0.01f0.005

# - x and y ticks for posterior on noise parameters (in GMT format)
#   (if set to "", ticks will be assigned automatically)
noise_x_ticks=a0.5f0.1
noise_y_ticks=a1f0.5

# - x and y ticks for actual residuals (in GMT format)
#   (if set to "", ticks will be assigned automatically)
resid_abs_x_ticks=a5f1
resid_abs_y_ticks=a1

# - x and y ticks for percentage residuals (in GMT format)
#   (if set to "", ticks will be assigned automatically)
resid_perc_x_ticks=a10f2
resid_perc_exp=3 # power of 10 for y axis of histogram
resid_perc_y_ticks=a1f0.5

######################################################################

# POINTS FOR POSTERIOR ON VELOCITY

# Choose points for which velocity posteriors should be calculated
# and y ticks for plot in GMT format (if set to "", ticks will be 
# assigned automatically)
# (points assumed to be on the xy plane where z=0)

number_of_points=4

p1_lat_y=0
p1_long_x=0
p1_y_ticks=a1f0.5

p2_lat_y=10
p2_long_x=0
p2_y_ticks=a2f1

p3_lat_y=20
p3_long_x=0
p3_y_ticks=a2f1

p4_lat_y=10
p4_long_x=10
p4_y_ticks=a5f1

######################################################################

# PARAMETERS FOR CHAIN EVOLUTION PLOTS

# How many independent Markov chains? (i.e. how many cores was the
# inversion run on in parallel?)
number_of_chains=4

# - x and y ticks for number of cells vs. iteration (in GMT format)
#   (if set to "", ticks will be assigned automatically)
chain_ncell_exp=3 # power of 10 for x axis
chain_ncell_x_ticks=a2f1
chain_ncell_y_ticks=a50f10

# - x and y ticks for number of cells vs. log10(misfit) (in GMT format)
#   (if set to "", ticks will be assigned automatically)
chain_misfits_exp=3 # power of 10 for x axis
chain_misfits_x_ticks=a2f1
chain_misfits_y_ticks=a1f0.5

######################################################################

# PARAMETERS FOR SYNTHETIC MODEL PLOTS

# Is this a synthetic test? ("yes" means it is)
synthetic_test=yes

# Do you want to plot the synthetic model? ("yes" means you do)
plot_synthetic=yes

# If it is a synthetic test, what is the real model file?
synthetic_directory=~/erica/TDTOMO/ForTSWT/examples/square_2d
synthetic_file=model1.vtx

######################################################################

# PARAMETERS FOR LINEARISED INVERSION PLOTS

# Plot inversion results from FMST? ("yes" means they are plotted)
plot_lin=no

# What are the linearised inversion files?
lin_directory=""
lin_vel_file=""
lin_unc_file=""

# Label and ticks for colorbar of uncertainty map
lin_unc_label="standard deviation (km s@+-1@+)"
lin_unc_ticks=a0.2f0.1

######################################################################

# GET INFORMATION FROM mksamples.in AND procsamples.in

# Define path to mksamples.in and procsamples.in
mksamples_in=${inversion_directory}/mksamples.in
procsamples_in=${inversion_directory}/procsamples.in

# Input files for inversion (from mksamples.in)
sources_file=`awk '{ if(NR==4) print $1}' ${mksamples_in}`
receivers_file=`awk '{ if(NR==5) print $1}' ${mksamples_in}`
data_file=`awk '{ if(NR==6) print $1}' ${mksamples_in}`
sphere_or_plane=`awk '{ if(NR==51) print $1}' ${mksamples_in}`
forward_file=`awk '{ if(NR==52) print $1}' ${mksamples_in}`
forward_file=${inversion_directory}/${forward_file}

# Output files from inversion (from mksamples.in)
ncell_file=`awk '{ if(NR==13) print $1}' ${mksamples_in}`
misfits_file=`awk '{ if(NR==15) print $1}' ${mksamples_in}`

# Output files from inversion and processing (from procsamples.in)
average_file=`awk '{ if(NR==14) print $1}' ${procsamples_in}`
stdev_file=`awk '{ if(NR==15) print $1}' ${procsamples_in}`
median_file=`awk '{ if(NR==16) print $1}' ${procsamples_in}`
maximum_file=`awk '{ if(NR==17) print $1}' ${procsamples_in}`
harmean_file=`awk '{ if(NR==18) print $1}' ${procsamples_in}`
rms_file=`awk '{ if(NR==19) print $1}' ${procsamples_in}`
entropy_file=`awk '{ if(NR==20) print $1}' ${procsamples_in}`
skewness_file=`awk '{ if(NR==21) print $1}' ${procsamples_in}`
kurtosis_file=`awk '{ if(NR==22) print $1}' ${procsamples_in}`
node_file=`awk '{ if(NR==28) print $1}' ${procsamples_in}`
first_file=`awk '{ if(NR==29) print $1}' ${procsamples_in}`
last_file=`awk '{ if(NR==30) print $1}' ${procsamples_in}`
ncellsi_file=`awk '{ if(NR==23) print $1}' ${procsamples_in}`
misfiti_file=`awk '{ if(NR==24) print $1}' ${procsamples_in}`
postncells_file=`awk '{ if(NR==25) print $1}' ${procsamples_in}`
postnoise_file=`awk '{ if(NR==26) print $1}' ${procsamples_in}`
postvalue_file=`awk '{ if(NR==27) print $1}' ${procsamples_in}`
residuals_histo_file=`awk '{ if(NR==31) print $1}' ${procsamples_in}`

# Prior (from mksamples.in)
y_min=`awk '{ if(NR==26) print $1}' ${mksamples_in}`
y_max=`awk '{ if(NR==26) print $2}' ${mksamples_in}`
x_min=`awk '{ if(NR==25) print $1}' ${mksamples_in}`
x_max=`awk '{ if(NR==25) print $2}' ${mksamples_in}`
ncell_min=`awk '{ if(NR==23) print $1}' ${mksamples_in}`
ncell_max=`awk '{ if(NR==23) print $2}' ${mksamples_in}`
value_min=`awk '{ if(NR==24) print $1}' ${mksamples_in}`
value_max=`awk '{ if(NR==24) print $2}' ${mksamples_in}`
number_of_datasets=`awk '{ if(NR==7) print $1}' ${mksamples_in}`
noise_type=`awk '{ if(NR==27) print $1}' ${mksamples_in}`
a_min=`awk '{ if(NR==28) print $1}' ${mksamples_in}`
a_max=`awk '{ if(NR==28) print $2}' ${mksamples_in}`
b_min=`awk '{ if(NR==29) print $1}' ${mksamples_in}`
b_max=`awk '{ if(NR==29) print $2}' ${mksamples_in}`
lambda_min=`awk '{ if(NR==30) print $1}' ${mksamples_in}`
lambda_max=`awk '{ if(NR==30) print $2}' ${mksamples_in}`
sigma_min=`awk '{ if(NR==31) print $1}' ${mksamples_in}`
sigma_max=`awk '{ if(NR==31) print $2}' ${mksamples_in}`

# Map pixelization parameters
x_number_of_pixels=`awk '{ if(NR==36) print $1}' ${procsamples_in}`
y_number_of_pixels=`awk '{ if(NR==36) print $2}' ${procsamples_in}`

# Parameters for raypath calculation (from forward_file)
long_x_nodes=`awk '{ if(NR==2) print $1}' ${forward_file}`
lat_y_nodes=`awk '{ if(NR==1) print $1}' ${forward_file}`
long_x_dicing=`awk '{ if(NR==4) print $1}' ${forward_file}`
lat_y_dicing=`awk '{ if(NR==3) print $1}' ${forward_file}`
apply_source_refinement=`awk '{ if(NR==5) print $1}' ${forward_file}`
refinement_dicing=`awk '{ if(NR==6) print $1}' ${forward_file}`
refinement_extent=`awk '{ if(NR==7) print $1}' ${forward_file}`
earth_radius=`awk '{ if(NR==10) print $1}' ${forward_file}`
first_or_mixed=`awk '{ if(NR==8) print $1}' ${forward_file}`
narrow_band_size=`awk '{ if(NR==9) print $1}' ${forward_file}`
