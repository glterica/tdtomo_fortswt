# TSWT - Transdimensional Surface Wave Tomography #

### Description ###

This repository contains part of the source code necessary to perform non-linear seismic traveltime tomography with the rj-McMC algorithm. 

It must be downloaded together with the companion repository [TDTOMO\_COMMON](https://bitbucket.org/glterica/tdtomo_common).

### How do I get set up? ###

You can download the latest version of the complete source code as a pre-packaged zip file by clicking [here](http://glterica.pythonanywhere.com/software/download/tswt).

Alternatively, if you wish to download/clone the repository, follow these steps: 

+ create a directory on your machine which will contain both this repository and the companion repository [TDTOMO\_COMMON](https://bitbucket.org/glterica/tdtomo_common)
+ download/clone this repository
+ download/clone the companion repository [TDTOMO\_COMMON](https://bitbucket.org/glterica/tdtomo_common), which contains the rest of the necessary source codes
+ rename the folder of the companion repository to "COMMON" to ensure all links in this repository work properly

The user guide in this repository ([```user_guide.pdf```](https://bitbucket.org/glterica/tdtomo_fortswt/src/master/user_guide.pdf)) provides instructions on how to compile the Fortran codes using either the Intel or GNU compilers, as well as a description of each program.
Example datasets are included in the [```examples```](https://bitbucket.org/glterica/tdtomo_fortswt/src/master/examples)) folder.

### Suggested reading ###

To learn about the theory behind non-linear and transdimensional tomography, we suggest reading the following papers:

+ Galetti E, Curtis A, Meles GA, Baptie B (2017) Transdimensional Love-wave tomography of the British Isles and shear-velocity structure of the East Irish Sea Basin from ambient-noise interferometry. *Geophysical Journal International* 208(1): 36-58. [DOI: 10.1093/gji/ggw286](http://dx.doi.org/10.1093/gji/ggw286). A copy is included in the [```papers```](https://bitbucket.org/glterica/tdtomo_fortswt/src/master/papers) folder.
+ Galetti E, Curtis A, Meles GA, Baptie B (2015) Uncertainty loops in travel-time tomography from nonlinear wave physics. *Physical Review Letters* 114: 148501. [DOI: 10.1103/PhysRevLett.114.148501](http://dx.doi.org/10.1103/PhysRevLett.114.148501). A copy is included in the [```papers```](https://bitbucket.org/glterica/tdtomo_fortswt/src/master/papers) folder.
+ Bodin T, Sambridge M, Rawlinson N, Arroucau P (2012) Transdimensional tomography with unknown data noise. *Geophysical Journal International* 189(3): 1536-1556. [DOI: 10.1111/j.1365-246X.2012.05414.x](http://dx.doi.org/10.1111/j.1365-246X.2012.05414.x)
+ Bodin T, Sambridge M (2009) Seismic tomography with the reversible jump algorithm. *Geophysical Journal International* 178(3): 1411-1436. [DOI: 10.1111/j.1365-246X.2009.04226.x](http://dx.doi.org/10.1111/j.1365-246X.2009.04226.x)

### Contact ###

If you would like to collaborate or contribute to this code, for questions and to report errors/bugs in the code or examples, you can contact:

+ Erica Galetti: [erica.galetti@ed.ac.uk](mailto:erica.galetti@ed.ac.uk)
+ Andrew Curtis: [Andrew.Curtis@ed.ac.uk](mailto:Andrew.Curtis@ed.ac.uk)
+ Both: [td.tomo.codes@gmail.com](mailto:td.tomo.codes@gmail.com)

***

### To-do list ###

+ Add option to use a custom random seed
+ Add option to save single samples in ```procsamples```
+ Clean up call to noise calculation routines from ```mksamples.f90```
+ Add option to average over neighbouring pixels for maximum-a-posteriori model in ```procsamples```

### Changelog ###

--

***

### License ###

This program is licensed under [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0-standalone.html). A copy of the license is available within this repository by clicking [here](https://bitbucket.org/glterica/tdtomo_fortswt/src/master/LICENSE.md).

### Credits ###

The programs in the TSWT package make use of the following libraries:

+ [FMST](http://rses.anu.edu.au/~nick/surftomo.html)
+ [mt19937](https://jblevins.org/mirror/amiller/mt19937.f90)
+ [ORDERPACK 2.0](http://www.fortran-2000.com/rank/index.html)
