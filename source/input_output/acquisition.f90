! ********************************************************************* !
! Module for reading data files (part of TSWT package)
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


MODULE acquisition

IMPLICIT NONE
CHARACTER (LEN=30), SAVE                                :: sources_file,receivers_file
CHARACTER (LEN=30), SAVE                                :: data_file,datasets_file
INTEGER, SAVE                                           :: number_of_sources,number_of_receivers
INTEGER, SAVE                                           :: number_of_measurements,number_of_datasets
DOUBLE PRECISION, ALLOCATABLE, SAVE                     :: sources(:,:),receivers(:,:)
INTEGER, DIMENSION(:,:), ALLOCATABLE, SAVE              :: source_receiver_pairs
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, SAVE       :: observed_data,data_noise
INTEGER, DIMENSION(:), ALLOCATABLE, SAVE                :: dataset_associations


CONTAINS


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE read_data(problem_type)

IMPLICIT NONE
INTEGER :: problem_type

CALL read_coordinates(sources_file,number_of_sources,sources)
CALL read_coordinates(receivers_file,number_of_receivers,receivers)
CALL read_measurements(problem_type)

END SUBROUTINE


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE read_coordinates(coordinates_file,number_of_elements,coordinates)
! Reads the files containing source/receiver coordinates. These files are structured as follows:
!
!       line 1          number of sources/receivers in array
!       line 2-end      source/receiver coordinates given as:
!                               column 1        source/receiver latitude (if spherical shell) OR
!                                               source/receiver x-coordinate (if Cartesian)
!                               column 2        source/receiver longitude (if spherical shell) OR
!                                               source/receiver y-coordinate (if Cartesian)
!                               column 3        source/receiver depth (if spherical shell) OR
!                                               source/receiver z-coordinate (if Cartesian)

IMPLICIT NONE
CHARACTER (LEN=30), INTENT(IN)                                  :: coordinates_file
INTEGER, INTENT(OUT)                                            :: number_of_elements
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT)      :: coordinates
DOUBLE PRECISION                                                :: x_coord,y_coord,z_coord
INTEGER                                                         :: nn,IOstatus
LOGICAL                                                         :: verbose=.FALSE.

IF (ALLOCATED(coordinates)) DEALLOCATE(coordinates)

OPEN(UNIT=10,FILE=coordinates_file,STATUS='OLD')
READ(10,*)number_of_elements
ALLOCATE(coordinates(number_of_elements,3)) ! check whether a depth/z-coordinate is given (file has 3 columns)
DO nn=1,number_of_elements
        READ(10,*,IOSTAT=IOstatus)x_coord,y_coord,z_coord
        coordinates(nn,:)=[x_coord,y_coord,z_coord]
END DO
IF (verbose) WRITE(*,*)'IOstatus=',IOstatus

IF (IOstatus.EQ.0) THEN ! if no error was encountered, a depth/z-coordinate is given (file has 3 columns)
        IF (verbose) WRITE(*,*)'Coordinates given in ',TRIM(coordinates_file),': x, y, z'
ELSEIF (IOstatus.NE.0) THEN ! in this case a depth/z-coordinate is not given (file has 2 columns)
        IF (verbose) WRITE(*,*)'Coordinates given in ',TRIM(coordinates_file),': x, y'
        REWIND(10)
        READ(10,*)
        DEALLOCATE(coordinates)
        ALLOCATE(coordinates(number_of_elements,2))
        DO nn=1,number_of_elements
                READ(10,*)x_coord,y_coord
                coordinates(nn,:)=[x_coord,y_coord]
        END DO
END IF
IF (verbose) WRITE(*,*)'IOstatus=',IOstatus
CLOSE(10)

END SUBROUTINE


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE read_measurements(problem_type)
! Reads the file containing traveltime measurements. This file is structured as follows:
!
!       line 1          number of measurements
!       line 2-end      measurements given as:
!                               column 1        switch (>0 if measurement is to be included in the inversion;
!                                               =0 if measurement should not be used in the inversion)
!                               column 2        measured traveltime (in seconds)
!                               column 3        uncertainty in measured traveltime (in seconds)

IMPLICIT NONE
INTEGER                                         :: problem_type
INTEGER                                         :: switch,ss,rr,dataset
DOUBLE PRECISION                                :: traveltime,error
INTEGER                                         :: nn,valid_count
INTEGER, DIMENSION(:,:), ALLOCATABLE            :: worki
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE     :: workd

IF (ALLOCATED(worki)) DEALLOCATE(worki)
IF (ALLOCATED(workd)) DEALLOCATE(workd)
IF (ALLOCATED(source_receiver_pairs)) DEALLOCATE(source_receiver_pairs)
IF (ALLOCATED(observed_data)) DEALLOCATE(observed_data)
IF (ALLOCATED(data_noise)) DEALLOCATE(data_noise)
IF (ALLOCATED(dataset_associations)) DEALLOCATE(dataset_associations)

OPEN(UNIT=10,FILE=data_file,STATUS='OLD')
number_of_measurements=number_of_sources*number_of_receivers
ALLOCATE(source_receiver_pairs(number_of_measurements,2))
IF (problem_type.EQ.0) THEN
        ALLOCATE(observed_data(number_of_measurements))
        ALLOCATE(data_noise(number_of_measurements))
        ALLOCATE(dataset_associations(number_of_measurements))
        IF (number_of_datasets.GT.1) THEN
                OPEN(UNIT=20,FILE=datasets_file,STATUS='OLD')
        ELSE IF (number_of_datasets.EQ.1) THEN
                dataset_associations=1
        END IF
END IF

valid_count=0
DO ss=1,number_of_sources
        DO rr=1,number_of_receivers
                IF (problem_type.EQ.0) READ(10,*)switch,traveltime,error ! inverse problem: we read the data
                IF (problem_type.EQ.1) READ(10,*)switch ! forward problem: we only need the raypath switches
                IF (number_of_datasets.GT.1.AND.problem_type.EQ.0) READ(20,*)dataset
                IF (switch.GT.0) THEN
                        valid_count=valid_count+1
                        source_receiver_pairs(valid_count,:)=[ss,rr]
                        IF (problem_type.EQ.0) THEN
                                observed_data(valid_count)=traveltime
                                data_noise(valid_count)=error
                                IF (number_of_datasets.GT.1) dataset_associations(valid_count)=dataset
                        END IF
                END IF
        END DO
END DO
CLOSE(10)
IF (number_of_datasets.GT.1.AND.problem_type.EQ.0) CLOSE(20)
IF (valid_count.LT.number_of_measurements) THEN
        number_of_measurements=valid_count ! number of valid readings
        ! Resize source_receiver_pairs to the number of valid readings
        ALLOCATE(worki(valid_count,2))
        worki=source_receiver_pairs(1:valid_count,:)
        DEALLOCATE(source_receiver_pairs)
        ALLOCATE(source_receiver_pairs(valid_count,2))
        source_receiver_pairs=worki
        DEALLOCATE(worki)
        IF (problem_type.EQ.0) THEN
                ! Resize observed_data to the number of valid readings
                ALLOCATE(workd(valid_count))
                workd=observed_data(1:valid_count)
                DEALLOCATE(observed_data)
                ALLOCATE(observed_data(valid_count))
                observed_data=workd
                ! Resize data_noise to the number of valid readings
                workd=data_noise(1:valid_count)
                DEALLOCATE(data_noise)
                ALLOCATE(data_noise(valid_count))
                data_noise=workd
                DEALLOCATE(workd)
                ! Resize dataset_associations to the number of valid readings
                ALLOCATE(worki(valid_count,1))
                worki(:,1)=dataset_associations(1:valid_count)
                DEALLOCATE(dataset_associations)
                ALLOCATE(dataset_associations(valid_count))
                dataset_associations=worki(:,1)
                DEALLOCATE(worki)
        END IF
END IF

END SUBROUTINE

END MODULE