#!/bin/sh

# Grid Engine Options
#$ -N mksamples
#$ -cwd
#$ -l h_rt=16:00:00
#$ -l h_vmem=2G
#$ -pe mpi 16
#$ -R y

# Initialise the environment
. /etc/profile
. /etc/profile.d/modules.sh

# Load the necessary modules
module load intel
module load openmpi/1.6.5
module load igmm/mpi/gcc/mpich/3.1.4

# Launch MPI executable
mpiexec -np $NSLOTS ./mksamples 1
