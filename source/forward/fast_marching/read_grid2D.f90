! ********************************************************************* !
! Subroutine to read forward modelling grid parameter file
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


SUBROUTINE read_grid2D(top,bottom,left,right)

USE globalp, ONLY: grid2D_file,nvx,nvz,gdx,gdz,asgr,sgdl,sgs,fom,snb,earth,&
                   goxd,gozd,dvxd,dvzd,lat_y_axis,long_x_axis,sphere_or_plane
IMPLICIT NONE
DOUBLE PRECISION, INTENT(IN)    :: top,bottom,left,right

OPEN(UNIT=10,FILE=grid2D_file,STATUS='old')

READ(10,*)nvx
READ(10,*)nvz
READ(10,*)gdx
READ(10,*)gdz
READ(10,*)asgr
READ(10,*)sgdl
READ(10,*)sgs
READ(10,*)fom
READ(10,*)snb
IF (sphere_or_plane.EQ.0) READ(10,*)earth

CLOSE(10)

goxd=top
gozd=left

ALLOCATE(lat_y_axis(nvx))
ALLOCATE(long_x_axis(nvz))

CALL linspace(bottom,top,nvx,lat_y_axis)
CALL linspace(left,right,nvz,long_x_axis)

dvxd=(top-bottom)/(nvx-1)
dvzd=(right-left)/(nvz-1)

END SUBROUTINE