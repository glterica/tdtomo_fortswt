! ********************************************************************* !
! Subroutine to initialise arrays required by fast_marching.f90
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


SUBROUTINE allocate_arrays()

USE acquisition
USE traveltime
USE globalp
IMPLICIT NONE
INTEGER         :: nn,ss,rr

! Array of sources
nsrc=number_of_sources
ALLOCATE(scx(nsrc),scz(nsrc))
IF (sphere_or_plane.EQ.0) THEN
        scx=sources(:,1) ! latitude
        scz=sources(:,2) ! longitude
        scx=(90.0-scx)*pi/180.0
        scz=scz*pi/180.0
ELSE IF (sphere_or_plane.EQ.1) THEN
        scx=sources(:,2) ! y-coordinate
        scz=sources(:,1) ! x-coordinate
END IF

! Array of receivers
nrc=number_of_receivers
ALLOCATE(rcx(nrc),rcz(nrc))
IF (sphere_or_plane.EQ.0) THEN
        rcx=receivers(:,1) ! latitude
        rcz=receivers(:,2) ! longitude
        rcx=(90.0-rcx)*pi/180.0
        rcz=rcz*pi/180.0
ELSE IF (sphere_or_plane.EQ.1) THEN
        rcx=receivers(:,2) ! y-coordinate
        rcz=receivers(:,1) ! x-coordinate
END IF

! Array of source-receiver path status
ALLOCATE(srs(nrc,nsrc))
srs=0
DO nn=1,number_of_measurements
        ss=source_receiver_pairs(nn,1)
        rr=source_receiver_pairs(nn,2)
        srs(rr,ss)=1
END DO

! Velocity field arrays
ALLOCATE(velv(0:nvz+1,0:nvx+1))
nnx=(nvx-1)*gdx+1
nnz=(nvz-1)*gdz+1
ALLOCATE(veln(nnz,nnx))
ALLOCATE(velnb(nnz,nnx))

! Arrays for velocity grid dicing
ALLOCATE(ui(gdx+1,4))
ALLOCATE(vi(gdz+1,4))

! Backup values
nnxb=nnx
nnzb=nnz
dnxb=dnx
dnzb=dnz
goxb=gox
gozb=goz

! Traveltime field array
ALLOCATE(ttn(nnz,nnx))
ALLOCATE(ttnr(nnzb,nnxb))
ALLOCATE(nstsr(nnzb,nnxb))

! Allocate memory for node status and binary trees
ALLOCATE(nsts(nnz,nnx))
maxbt=NINT(snb*nnx*nnz)
ALLOCATE(btg(maxbt))

! Raypath coordinates
maxrp=nnx*nnz
ALLOCATE(rgx(maxrp+1),rgz(maxrp+1))

! Traveltime measurements and raypath lengths
tnr=number_of_measurements

END SUBROUTINE