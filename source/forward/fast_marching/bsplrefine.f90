! This subroutine was taken from the standalone
! program fm2dss.f90 by Nick Rawlinson.
! See FMST page for details:
! http://rses.anu.edu.au/~nick/surftomo.html

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! TYPE: SUBROUTINE
! CODE: FORTRAN 90
! This subroutine is similar to bsplreg except that it has been
! modified to deal with source grid refinement
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE bsplrefine
USE globalp
INTEGER :: i,j,k,l,i1,j1,st1,st2,nrzr,nrxr
INTEGER :: origx,origz,conx,conz,idm1,idm2
DOUBLE PRECISION :: u,v
DOUBLE PRECISION, DIMENSION (4) :: sum
DOUBLE PRECISION, DIMENSION(gdx*sgdl+1,gdz*sgdl+1,4) :: ui2,vi2
!
! nrxr,nrzr = grid refinement level for source grid in x,z
! origx,origz = local origin of refined source grid
!
! Begin by calculating the values of the basis functions
!
nrxr=gdx*sgdl
nrzr=gdz*sgdl
DO i=1,nrzr+1
   v=nrzr
   v=(i-1)/v
   DO j=1,nrxr+1
      u=nrxr
      u=(j-1)/u
      ui2(j,i,1)=(1.0-u)**3/6.0
      ui2(j,i,2)=(4.0-6.0*u**2+3.0*u**3)/6.0
      ui2(j,i,3)=(1.0+3.0*u+3.0*u**2-3.0*u**3)/6.0
      ui2(j,i,4)=u**3/6.0
      vi2(j,i,1)=(1.0-v)**3/6.0
      vi2(j,i,2)=(4.0-6.0*v**2+3.0*v**3)/6.0
      vi2(j,i,3)=(1.0+3.0*v+3.0*v**2-3.0*v**3)/6.0
      vi2(j,i,4)=v**3/6.0
   ENDDO
ENDDO
!
! Calculate the velocity values.
!
origx=(vnl-1)*sgdl+1
origz=(vnt-1)*sgdl+1
DO i=1,nvz-1
   conz=nrzr
   IF(i==nvz-1)conz=nrzr+1
   DO j=1,nvx-1
      conx=nrxr
      IF(j==nvx-1)conx=nrxr+1
      DO k=1,conz
         st1=gdz*(i-1)+(k-1)/sgdl+1
         IF(st1.LT.vnt.OR.st1.GT.vnb)CYCLE
         st1=nrzr*(i-1)+k
         DO l=1,conx
            st2=gdx*(j-1)+(l-1)/sgdl+1
            IF(st2.LT.vnl.OR.st2.GT.vnr)CYCLE
            st2=nrxr*(j-1)+l
            DO i1=1,4
               sum(i1)=0.0
               DO j1=1,4
                  sum(i1)=sum(i1)+ui2(l,k,j1)*velv(i-2+i1,j-2+j1)
               ENDDO
               sum(i1)=vi2(l,k,i1)*sum(i1)
            ENDDO
            idm1=st1-origz+1
            idm2=st2-origx+1
            IF(idm1.LT.1.OR.idm1.GT.nnz)CYCLE
            IF(idm2.LT.1.OR.idm2.GT.nnx)CYCLE
            veln(idm1,idm2)=sum(1)+sum(2)+sum(3)+sum(4)
         ENDDO
      ENDDO
   ENDDO
ENDDO
END SUBROUTINE bsplrefine