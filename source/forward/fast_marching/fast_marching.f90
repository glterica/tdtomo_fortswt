! ********************************************************************* !
! Forward modelling module for surface wave traveltime calculation
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


MODULE fast_marching

USE acquisition
USE globalp
USE traveltime
USE string_conversion


CONTAINS


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE initialize_fm()

IMPLICIT NONE
INTERFACE
        SUBROUTINE read_grid2D(top,bottom,left,right)
                DOUBLE PRECISION, INTENT(IN)    :: top,bottom,left,right
        END SUBROUTINE
END INTERFACE
INTERFACE
        SUBROUTINE allocate_arrays()
        END SUBROUTINE
END INTERFACE
INTERFACE
        SUBROUTINE initialize_gridder()
        END SUBROUTINE
END INTERFACE

CALL read_grid2D(north,south,west,east)

CALL allocate_arrays()

CALL initialize_gridder()

END SUBROUTINE


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE run_fmmin2d(traveltimes,info,raypath_lengths)

! This subroutine was adapted from the standalone program
! fm2dss.f90 by Nick Rawlinson

IMPLICIT NONE
DOUBLE PRECISION, DIMENSION(number_of_measurements), INTENT(OUT) :: traveltimes
INTEGER, INTENT(OUT) :: info
DOUBLE PRECISION, DIMENSION(number_of_measurements), INTENT(OUT), OPTIONAL :: raypath_lengths
INTEGER :: i,j,k,l
INTEGER :: wttf,fsrt,wrgf,cfd,urg
INTEGER :: isx,isz,sw,idm1,idm2
INTEGER :: ogx,ogz,grdfx,grdfz
DOUBLE PRECISION :: x,z
DOUBLE PRECISION, DIMENSION(nrc) :: source_traveltimes
DOUBLE PRECISION, DIMENSION(nrc) :: source_ray_lengths
INTERFACE
        SUBROUTINE apply_gridder()
        END SUBROUTINE
END INTERFACE

! wttf = Write traveltimes to file? (0=no,>0=source id)
! fsrt = find source-receiver traveltimes? (0=no,1=yes)
! wrgf = write ray geometries to file? (<0=all,0=no,>0=source id.)
! cfd = calculate Frechet derivatives? (0=no, 1=yes)
! urg = use refined grid (0=no,1=yes,2=previously used)
! isx,isz = cell containing source
! x,z = temporary variables for source location
! ogx,ogz = Location of refined grid origin
! gridfx,grdfz = Number of refined nodes per cell

info=0
rbint=0

CALL apply_gridder()

IF (.NOT.ALLOCATED(nsts).OR.SIZE(nsts,1).NE.nnz.OR.SIZE(nsts,2).NE.nnx) THEN
        IF (ALLOCATED(nsts)) DEALLOCATE(nsts)
        ALLOCATE(nsts(nnz,nnx))
END IF
maxbt=NINT(snb*nnx*nnz)
IF (.NOT.ALLOCATED(btg).OR.SIZE(btg,1).NE.maxbt) THEN
        IF (ALLOCATED(btg)) DEALLOCATE(btg)
        ALLOCATE(btg(maxbt))
END IF

! Start counter for source-receiver paths
path_number=0
path_number0=0

DO i=1,nsrc
        
        ! Source coordinates
        x=scx(i) ! source latitude or y-coordinate
        z=scz(i) ! source longitude or x-coordinate
        
        ! Begin by computing refined source grid if required
        urg=0
        IF (asgr.EQ.1) THEN
                
                ! Back up coarse velocity grid to a holding matrix
                !IF (i.EQ.1) ALLOCATE(velnb(nnz,nnx))
                velnb=veln
                nnxb=nnx
                nnzb=nnz
                dnxb=dnx
                dnzb=dnz
                goxb=gox
                gozb=goz
                
                ! Identify nearest neighbouring node to source
                IF (sphere_or_plane.EQ.0) THEN
                        isx=INT((x-gox)/dnx)+1
                ELSE IF (sphere_or_plane.EQ.1) THEN
                        isx=INT((gox-x)/dnx)+1
                END IF
                isz=INT((z-goz)/dnz)+1
                sw=0
                IF (isx.LT.1.OR.isx.GT.nnx) sw=1
                IF (isz.LT.1.OR.isz.GT.nnz) sw=1
                IF (sw.EQ.1) THEN
                        WRITE(*,*)
                        WRITE(*,*)'********************************************'
                        WRITE(*,*)'ERROR in subroutine fmmin2d'
                        WRITE(*,*)'--------------------------------------------'
                        WRITE(*,*)'Source ',TRIM(int2str(i)),' lies outside of model:'
                        IF (sphere_or_plane.EQ.0) WRITE(*,*)'    lat = ',TRIM(dble2str((pi/2-x)*180.0/pi))
                        IF (sphere_or_plane.EQ.0) WRITE(*,*)'   long = ',TRIM(dble2str(z*180.0/pi))
                        IF (sphere_or_plane.EQ.1) WRITE(*,*)'    x = ',TRIM(dble2str(z))
                        IF (sphere_or_plane.EQ.1) WRITE(*,*)'    y = ',TRIM(dble2str(x))
                        WRITE(*,*)'Check input files.'
                        WRITE(*,*)'TERMINATING PROGRAM!!'
                        WRITE(*,*)'********************************************'
                        WRITE(*,*)
                        STOP
                ENDIF
                IF (isx.EQ.nnx) isx=isx-1
                IF (isz.EQ.nnz) isz=isz-1
                
                ! Now find rectangular box that extends outward from the nearest source node
                ! to "sgs" nodes away.
                vnl=isx-sgs
                IF (vnl.LT.1) vnl=1
                vnr=isx+sgs
                IF (vnr.GT.nnx) vnr=nnx
                vnt=isz-sgs
                IF (vnt.LT.1) vnt=1
                vnb=isz+sgs
                IF (vnb.GT.nnz) vnb=nnz
                nrnx=(vnr-vnl)*sgdl+1
                nrnz=(vnb-vnt)*sgdl+1
                drnx=dvx/DBLE(gdx*sgdl)
                drnz=dvz/DBLE(gdz*sgdl)
                IF (sphere_or_plane.EQ.0) THEN
                        gorx=gox+dnx*(vnl-1)
                ELSE IF (sphere_or_plane.EQ.1) THEN
                        gorx=gox-dnx*(vnl-1)
                END IF
                gorz=goz+dnz*(vnt-1)
                nnx=nrnx
                nnz=nrnz
                dnx=drnx
                dnz=drnz
                gox=gorx
                goz=gorz
                
                ! Reallocate velocity and traveltime arrays if nnx>nnxb or
                ! nnz<nnzb.
                IF (nnx.GT.nnxb.OR.nnz.GT.nnzb) THEN
                        !WRITE(*,*)'***** REALLOCATING 1 *****'
                        idm1=nnx
                        IF (nnxb.GT.idm1) idm1=nnxb
                        idm2=nnz
                        IF (nnzb.GT.idm2) idm2=nnzb
                        DEALLOCATE(veln,ttn,nsts,btg)
                        ALLOCATE(veln(idm2,idm1))
                        ALLOCATE(ttn(idm2,idm1))
                        ALLOCATE(nsts(idm2,idm1))
                        maxbt=NINT(snb*idm1*idm2)
                        ALLOCATE(btg(maxbt))
                END IF
        
                ! Call a subroutine to compute values of refined velocity nodes
                CALL bsplrefine
                
                ! Compute first-arrival traveltime field through refined grid.
                urg=1
                CALL travel(x,z,urg,i)
                
                ! Now map refined grid onto coarse grid.
                IF (.NOT.ALLOCATED(ttnr).OR.SIZE(ttnr,1).NE.nnzb.OR.SIZE(ttnr,2).NE.nnxb) THEN
                        IF (ALLOCATED(ttnr)) DEALLOCATE(ttnr)
                        IF (ALLOCATED(nstsr)) DEALLOCATE(nstsr)
                        ALLOCATE(ttnr(nnzb,nnxb))
                        ALLOCATE(nstsr(nnzb,nnxb))
                END IF
                IF (nnx.GT.nnxb.OR.nnz.GT.nnzb) THEN
                        !WRITE(*,*)'***** REALLOCATING 2 *****'
                        idm1=nnx
                        IF (nnxb.GT.idm1) idm1=nnxb
                        idm2=nnz
                        IF (nnzb.GT.idm2) idm2=nnzb
                        DEALLOCATE(ttnr,nstsr)
                        ALLOCATE(ttnr(idm2,idm1))
                        ALLOCATE(nstsr(idm2,idm1))
                END IF
                ttnr=ttn
                nstsr=nsts
                ogx=vnl
                ogz=vnt
                grdfx=sgdl
                grdfz=sgdl
                nsts=-1
                DO k=1,nnz,grdfz
                        idm1=ogz+(k-1)/grdfz
                        DO l=1,nnx,grdfx
                                idm2=ogx+(l-1)/grdfx
                                nsts(idm1,idm2)=nstsr(k,l)
                                IF(nsts(idm1,idm2).GE.0)THEN
                                        ttn(idm1,idm2)=ttnr(k,l)
                                END IF
                        END DO
                END DO
                
                ! Backup refined grid information
                nnxr=nnx
                nnzr=nnz
                goxr=gox
                gozr=goz
                dnxr=dnx
                dnzr=dnz
                
                ! Restore remaining values.
                nnx=nnxb
                nnz=nnzb
                dnx=dnxb
                dnz=dnzb
                gox=goxb
                goz=gozb
                DO j=1,nnx
                        DO k=1,nnz 
                                veln(k,j)=velnb(k,j)
                        END DO
                END DO
        
                ! Ensure that the narrow band is complete; if
                ! not, then some alive points will need to be
                ! made close.
                DO k=1,nnx
                        DO l=1,nnz
                                IF (nsts(l,k).EQ.0) THEN
                                        IF (l-1.GE.1) THEN
                                                IF (nsts(l-1,k).EQ.-1) nsts(l,k)=1
                                        END IF
                                        IF (l+1.LE.nnz) THEN
                                                IF (nsts(l+1,k).EQ.-1) nsts(l,k)=1
                                        END IF
                                        IF (k-1.GE.1) THEN
                                                IF (nsts(l,k-1).EQ.-1) nsts(l,k)=1
                                        END IF
                                        IF (k+1.LE.nnx) THEN
                                                IF (nsts(l,k+1).EQ.-1) nsts(l,k)=1
                                        END IF
                                END IF
                        END DO
                END DO
                
                ! Finally, call routine for computing traveltimes once
                ! again.
                urg=2
                CALL travel(x,z,urg,i)
        
        ELSE
        
                ! Call a subroutine that works out the first-arrival traveltime
                ! field.
                CALL travel(x,z,urg,i)
        
        END IF
        
        !  Find source-receiver traveltimes
        CALL srtimes(x,z,i,source_traveltimes)
        
        ! Calculate raypath lengths if required
        IF (calculate_paths.EQ.1) THEN
                CALL rpaths(i,x,z,source_ray_lengths,info)
                IF (info.EQ.1) THEN
                        !IF (asgr.EQ.1) DEALLOCATE(ttnr,nstsr)
                        RETURN
                END IF
        END IF
        
        ! Merge traveltimes and raypath lengths for this source with the total arrays
        IF (path_number.NE.0.AND.path_number.NE.path_number0) THEN
                traveltimes(path_number0+1:path_number)=source_traveltimes(1:srtimes_counter)
                IF (calculate_paths.EQ.1) raypath_lengths(path_number0+1:path_number)=source_ray_lengths(1:rpaths_counter)
                path_number0=path_number
        END IF
               
        !IF (asgr.EQ.1) DEALLOCATE(ttnr,nstsr)

END DO

!  Notify about ray-boundary intersections if required.
IF(rbint.EQ.1)THEN
        WRITE(*,*)'Note that at least one two-point ray path'
        WRITE(*,*)'tracked along the boundary of the model.'
        WRITE(*,*)'This class of path is unlikely to be'
        WRITE(*,*)'a true path, and it is STRONGLY RECOMMENDED'
        WRITE(*,*)'that you adjust the dimensions of your grid'
        WRITE(*,*)'to prevent this from occurring.'
ENDIF

END SUBROUTINE

END MODULE