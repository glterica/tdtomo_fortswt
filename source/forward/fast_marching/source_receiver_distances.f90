! ********************************************************************* !
! Subroutine to calculate source-receiver distance
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


SUBROUTINE source_receiver_distances()

USE chains, ONLY: noise_x_current,noise_x_proposed,noise_x_delayed
USE globalp, ONLY: sphere_or_plane,scx,scz,rcx,rcz,pi
USE acquisition, ONLY: source_receiver_pairs,number_of_measurements
IMPLICIT NONE
DOUBLE PRECISION, DIMENSION(number_of_measurements)     :: distances
DOUBLE PRECISION, DIMENSION(number_of_measurements)     :: lat_y_source,long_x_source
DOUBLE PRECISION, DIMENSION(number_of_measurements)     :: lat_y_receiver,long_x_receiver

lat_y_source=scx(source_receiver_pairs(:,1))
long_x_source=scz(source_receiver_pairs(:,1))
lat_y_receiver=rcx(source_receiver_pairs(:,2))
long_x_receiver=rcz(source_receiver_pairs(:,2))

IF (sphere_or_plane.EQ.0) THEN
        lat_y_source=(pi/2-lat_y_source)*180.0/pi
        lat_y_source=lat_y_source*pi/180
        lat_y_receiver=(pi/2-lat_y_receiver)*180.0/pi
        lat_y_receiver=lat_y_receiver*pi/180
        distances=((2*ASIN(SQRT((SIN((lat_y_source-lat_y_receiver)/2))**2&
                               +(COS(lat_y_receiver))*(COS(lat_y_source))&
                               *(SIN((long_x_source-long_x_receiver)/2))**2)))&
                               *180.0/pi)
ELSE IF (sphere_or_plane.EQ.1) THEN
        distances=SQRT((lat_y_source-lat_y_receiver)**2&
                      +(long_x_source-long_x_receiver)**2)
        distances=distances/100
END IF

noise_x_current=distances
noise_x_proposed=distances
noise_x_delayed=distances

END SUBROUTINE