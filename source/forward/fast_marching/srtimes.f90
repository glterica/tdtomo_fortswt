! This subroutine was adapted from the standalone
! program fm2dss.f90 by Nick Rawlinson.
! See FMST page for details:
! http://rses.anu.edu.au/~nick/surftomo.html

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! TYPE: SUBROUTINE
! CODE: FORTRAN 90
! This subroutine calculates all receiver traveltimes for
! a given source.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE srtimes(cscx,cscz,csid,traveltimes)
USE globalp
USE string_conversion
IMPLICIT NONE
INTEGER, INTENT(IN) :: csid
DOUBLE PRECISION, INTENT(IN) :: cscx,cscz
DOUBLE PRECISION, DIMENSION(nrc), INTENT(OUT) :: traveltimes
INTEGER :: irx,irz,isx,isz
DOUBLE PRECISION :: trr
DOUBLE PRECISION :: drx,drz,produ
INTEGER :: i,k,l,sw
DOUBLE PRECISION :: sred,dpl,rd1
DOUBLE PRECISION :: vels,velr
DOUBLE PRECISION, DIMENSION (2,2) :: vss
!
! csid = current source ID
! cscx,cscz = current source coordinates
! irx,irz = Coordinates of cell containing receiver
! isx,isz = source cell location
! trr = traveltime value at receiver
! drx,drz = receiver distance from (i,j,k) grid node
! produ = dummy multiplier
! sred = Distance from source to receiver
! dpl = Minimum path length in source neighbourhood.
! vels,velr = velocity at source and receiver
! vss = velocity at four grid points about source or receiver
!
! Determine source-receiver traveltimes one at a time.
!
traveltimes=0
srtimes_counter=0
DO i=1,nrc
   IF (srs(i,csid).EQ.0) THEN
      CYCLE
   ELSE IF (srs(i,csid).EQ.1) THEN
      path_number=path_number+1
      srtimes_counter=srtimes_counter+1
   ENDIF
! 
!  The first step is to locate the receiver in the grid.
!
   IF (sphere_or_plane.EQ.0) THEN
      irx=INT((rcx(i)-gox)/dnx)+1
   ELSE IF (sphere_or_plane.EQ.1) THEN
      irx=INT((gox-rcx(i))/dnx)+1
   END IF
   irz=INT((rcz(i)-goz)/dnz)+1
   sw=0
   IF(irx.lt.1.or.irx.gt.nnx)sw=1
   IF(irz.lt.1.or.irz.gt.nnz)sw=1
   IF(sw.eq.1)then
      WRITE(*,*)
      WRITE(*,*)'********************************************'
      WRITE(*,*)'ERROR in subroutine srtimes'
      WRITE(*,*)'--------------------------------------------'
      WRITE(*,*)'Receiver ',TRIM(int2str(i)),' lies outside of model:'
      IF (sphere_or_plane.EQ.0) WRITE(*,*)'    lat = ',TRIM(dble2str((pi/2-rcx(i))*180.0/pi))
      IF (sphere_or_plane.EQ.0) WRITE(*,*)'   long = ',TRIM(dble2str(rcz(i)*180.0/pi))
      IF (sphere_or_plane.EQ.1) WRITE(*,*)'    x = ',TRIM(dble2str(rcz(i)))
      IF (sphere_or_plane.EQ.1) WRITE(*,*)'    y = ',TRIM(dble2str(rcx(i)))
      WRITE(*,*)'Check input files.'
      WRITE(*,*)'TERMINATING PROGRAM!!'
      WRITE(*,*)'********************************************'
      WRITE(*,*)
      STOP
   ENDIF
   IF(irx.eq.nnx)irx=irx-1
   IF(irz.eq.nnz)irz=irz-1
!
!  Location of receiver successfully found within the grid. Now approximate
!  traveltime at receiver using bilinear interpolation from four
!  surrounding grid points. Note that bilinear interpolation is a poor
!  approximation when traveltime gradient varies significantly across a cell,
!  particularly near the source. Thus, we use an improved approximation in this
!  case. First, locate current source cell.
!
   IF (sphere_or_plane.EQ.0) THEN
      isx=INT((cscx-gox)/dnx)+1
   ELSE IF (sphere_or_plane.EQ.1) THEN
      isx=INT((gox-cscx)/dnx)+1
   END IF
   isz=INT((cscz-goz)/dnz)+1
   IF (sphere_or_plane.EQ.0) THEN
      dpl=dnx*earth
      rd1=dnz*earth*SIN(gox)
   ELSE IF (sphere_or_plane.EQ.1) THEN
      dpl=dnx
      rd1=dnz
   END IF
   IF(rd1.LT.dpl)dpl=rd1
   IF (sphere_or_plane.EQ.0) THEN
      rd1=dnz*earth*SIN(gox+(nnx-1)*dnx)
   ELSE IF (sphere_or_plane.EQ.1) THEN
      rd1=dnz
   END IF
   IF(rd1.LT.dpl)dpl=rd1
   IF (sphere_or_plane.EQ.0) THEN
      sred=((cscx-rcx(i))*earth)**2
      sred=sred+((cscz-rcz(i))*earth*SIN(rcx(i)))**2
   ELSE IF (sphere_or_plane.EQ.1) THEN
      sred=(cscx-rcx(i))**2
      sred=sred+(cscz-rcz(i))**2
   END IF
   sred=SQRT(sred)
   IF(sred.LT.dpl)sw=1
   IF(isx.EQ.irx)THEN
      IF(isz.EQ.irz)sw=1
   ENDIF
   IF(sw.EQ.1)THEN
!
!     Compute velocity at source and receiver
!
      DO k=1,2
         DO l=1,2
            vss(k,l)=veln(isz-1+l,isx-1+k)
         ENDDO
      ENDDO
      IF (sphere_or_plane.EQ.0) THEN
         drx=(cscx-gox)-(isx-1)*dnx
      ELSE IF (sphere_or_plane.EQ.1) THEN
         drx=(gox-cscx)-(isx-1)*dnx
      END IF
      drz=(cscz-goz)-(isz-1)*dnz
      CALL bilinear(vss,drx,drz,vels)
      DO k=1,2
         DO l=1,2
            vss(k,l)=veln(irz-1+l,irx-1+k)
         ENDDO
      ENDDO
      IF (sphere_or_plane.EQ.0) THEN
         drx=(rcx(i)-gox)-(irx-1)*dnx
      ELSE IF (sphere_or_plane.EQ.1) THEN
         drx=(gox-rcx(i))-(irx-1)*dnx
      END IF
      drz=(rcz(i)-goz)-(irz-1)*dnz
      CALL bilinear(vss,drx,drz,velr)
      trr=2.0*sred/(vels+velr)
   ELSE
      IF (sphere_or_plane.EQ.0) THEN
         drx=(rcx(i)-gox)-(irx-1)*dnx
      ELSE IF (sphere_or_plane.EQ.1) THEN
         drx=(gox-rcx(i))-(irx-1)*dnx
      END IF
      drz=(rcz(i)-goz)-(irz-1)*dnz
      trr=0.0
      DO k=1,2
         DO l=1,2
            produ=(1.0-ABS(((l-1)*dnz-drz)/dnz))*(1.0-ABS(((k-1)*dnx-drx)/dnx))
            trr=trr+ttn(irz-1+l,irx-1+k)*produ
         ENDDO
      ENDDO
   ENDIF
   traveltimes(srtimes_counter)=trr
ENDDO
END SUBROUTINE srtimes