! This module was adapted from the standalone
! program fm2dss.f90 by Nick Rawlinson.
! See FMST page for details:
! http://rses.anu.edu.au/~nick/surftomo.html

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! TYPE: MODULE
! CODE: FORTRAN 90
! This module declares variable for global use, that is, for
! USE in any subroutine or function or other module. 
! Variables whose values are SAVEd can have their most
! recent values reused in any routine.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE globalp
IMPLICIT NONE
INTEGER, SAVE :: nvx,nvz,nnx,nnz,nnxr,nnzr,nrnx,nrnz
DOUBLE PRECISION, SAVE :: gox,goz,goxr,gozr
DOUBLE PRECISION, SAVE :: dvx,dvz,dnx,dnz,dnxr,dnzr,drnx,drnz
INTEGER, SAVE :: gdx,gdz
DOUBLE PRECISION, SAVE :: goxd,gozd,dvxd,dvzd,dnxd,dnzd,gorx,gorz
INTEGER, SAVE :: nnxb,nnzb
DOUBLE PRECISION, SAVE :: goxb,gozb,dnxb,dnzb
DOUBLE PRECISION, DIMENSION (:,:), ALLOCATABLE, SAVE :: velv,veln,velnb
DOUBLE PRECISION, DIMENSION (:,:), ALLOCATABLE, SAVE :: ttn,ttnr
INTEGER, DIMENSION (:,:), ALLOCATABLE, SAVE :: nsts,nstsr
INTEGER, SAVE :: vnl,vnr,vnt,vnb
INTEGER, SAVE :: rbint
INTEGER, DIMENSION (:,:), ALLOCATABLE, SAVE :: srs
INTEGER, SAVE :: nsrc,nrc
DOUBLE PRECISION, DIMENSION (:), ALLOCATABLE, SAVE :: scx,scz
DOUBLE PRECISION, DIMENSION (:), ALLOCATABLE, SAVE :: rcx,rcz
INTEGER, SAVE :: tnr
DOUBLE PRECISION, DIMENSION (:), ALLOCATABLE, SAVE :: rgx,rgz
INTEGER, SAVE :: maxrp
INTEGER, SAVE :: asgr,sgdl,sgs
INTEGER, SAVE :: fom
DOUBLE PRECISION, SAVE :: snb,earth
INTEGER, SAVE :: maxbt
DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE, SAVE :: ui,vi

CHARACTER (LEN=30), SAVE :: grid2D_file
DOUBLE PRECISION, SAVE :: north,south,east,west
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE, SAVE :: lat_y_axis,long_x_axis
INTEGER, SAVE :: sphere_or_plane
DOUBLE PRECISION, SAVE :: ray_length
INTEGER, SAVE :: calculate_paths
INTEGER, SAVE :: path_number,path_number0,srtimes_counter,rpaths_counter

DOUBLE PRECISION, PARAMETER :: pi=DBLE(4)*ATAN(DBLE(1))
!
! nvx,nvz = B-spline vertex values
! nnx,nnz = Number of nodes of grid in x and z
! nnxr,nnzr = Number of nodes of refined grid in x and z
! nrnx,nrnz = Number of nodes in x and z for refined grid
! gox,goz = Origin of grid (theta,phi)
! goxr,gozr = Origin of refined grid (theta,phi)
! gorx,gorz = Grid origin of refined grid
! dvx,dvz = B-spline vertex separation
! dnx,dnz = Node separation of grid in x and z
! dnxr,dnzr = Node separation of refined grid in x and z
! gdx,gdz = grid dicing in x and z
! goxd,gozd = gox,goz in degrees
! dvxd,dvzd = dvx,dvz in degrees
! dnxd,dnzd = dnx,dnz in degrees
! nnxb,nnzb = Backup for nnz,nnx
! goxb,gozb = Backup for gox,goz
! dnxb,dnzb = Backup for dnx,dnz
! velv(i,j) = velocity values at control points
! veln(i,j) = velocity values on a refined grid of nodes
! velnb(i,j) = Backup of veln required for source grid refinement
! ttn(i,j) = traveltime field on the refined grid of nodes
! ttnr(i,j) = ttn for refined grid
! nsts(i,j) = node status (-1=far,0=alive,>0=close)
! nstsr(i,j) = nsts for refined grid
! vnl,vnr,vnb,vnt = Bounds of refined grid
! rbint = Ray-boundary intersection (0=no,1=yes)
! srs = Source-receiver status (0=no path, 1=path exists)
! nsrc = number of sources
! scx,scz = (x,z) coordinates of sources
! nrc = number of receivers
! rcx,rcz = (x,z) coordinates of receivers
! tnr = total number of rays
! rgx,rgz = (x,z) coordinates of ray geometry
! maxrp = maximum number of ray points
! asgr = Apply source grid refinement (0=no,1=yes)
! sgdl = Source grid dicing level
! sgs = Extent of refined source grid
! fom = use first-order(0) or mixed-order(1) scheme
! snb = Maximum size of narrow band as fraction of nnx*nnz
! earth = radius of Earth (in km)
! maxbt = maximum size of narrow band binary tree
! ui,vi = bspline basis functions
!
END MODULE globalp
