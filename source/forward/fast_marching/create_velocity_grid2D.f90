! ********************************************************************* !
! Subroutine to create a 2D velocity grid from a Voronoi model
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


SUBROUTINE create_velocity_grid2D(ncell,voronoi_model)

USE fast_marching, ONLY: velv,lat_y_axis,long_x_axis,nvx,nvz
USE voronoi_operations
IMPLICIT NONE
INTEGER, INTENT(IN)                                     :: ncell
DOUBLE PRECISION, DIMENSION(ncell,3), INTENT(IN)        :: voronoi_model
INTEGER                                                 :: ii,jj,i,j
DOUBLE PRECISION, DIMENSION(ncell,4)                    :: voronoi_temp
DOUBLE PRECISION, DIMENSION(3)                          :: point
INTEGER                                                 :: nucleus

voronoi_temp(:,1)=voronoi_model(1:ncell,1)
voronoi_temp(:,2)=voronoi_model(1:ncell,2)
voronoi_temp(:,3)=0
voronoi_temp(:,4)=voronoi_model(1:ncell,3)

DO j=0,nvx+1
        jj=nvx-j+1
        IF (j.EQ.0) jj=nvx
        IF (j.EQ.nvx+1) jj=1
        DO i=0,nvz+1
                ii=i
                IF (i.EQ.0) ii=1
                IF (i.EQ.nvz+1) ii=nvz
                point=(/long_x_axis(ii),lat_y_axis(jj),DBLE(0)/)
                CALL find_closest_nucleus(point,voronoi_temp(:,1:3),ncell,nucleus)
                velv(i,j)=voronoi_temp(nucleus,4)
        END DO
END DO

END SUBROUTINE