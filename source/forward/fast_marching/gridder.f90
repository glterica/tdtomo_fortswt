! ********************************************************************* !
! Subroutines to initialise forward-modelling grids
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


! These subroutines were adapted from the standalone
! program fm2dss.f90 by Nick Rawlinson.
! See FMST page for details:
! http://rses.anu.edu.au/~nick/surftomo.html


SUBROUTINE initialize_gridder()

USE globalp
IMPLICIT NONE
INTEGER :: i
DOUBLE PRECISION :: u
! u = independent parameter for b-spline

IF (sphere_or_plane.EQ.0) THEN
        ! Convert from degrees to radians
        dvx=dvxd*pi/180.0
        dvz=dvzd*pi/180.0
        gox=(90.0-goxd)*pi/180.0
        goz=gozd*pi/180.0
ELSE IF (sphere_or_plane.EQ.1) THEN
        ! Leave Cartesian coordinates as they are
        dvx=dvxd
        dvz=dvxd
        gox=goxd
        goz=gozd
END IF

! Compute grid spacing for thinner velocity grid
dnx=dvx/gdx
dnz=dvz/gdz
dnxd=dvxd/gdx
dnzd=dvzd/gdz

! Calculate b-spline basis functions ui and vi
DO i=1,gdx+1
   u=gdx
   u=(i-1)/u
   ui(i,1)=(1.0-u)**3/6.0
   ui(i,2)=(4.0-6.0*u**2+3.0*u**3)/6.0
   ui(i,3)=(1.0+3.0*u+3.0*u**2-3.0*u**3)/6.0
   ui(i,4)=u**3/6.0
ENDDO
DO i=1,gdz+1
   u=gdz
   u=(i-1)/u
   vi(i,1)=(1.0-u)**3/6.0
   vi(i,2)=(4.0-6.0*u**2+3.0*u**3)/6.0
   vi(i,3)=(1.0+3.0*u+3.0*u**2-3.0*u**3)/6.0
   vi(i,4)=u**3/6.0
ENDDO

END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE apply_gridder()

USE globalp
IMPLICIT NONE
INTEGER :: conx,conz
INTEGER :: stx,stz
DOUBLE PRECISION :: sumi,sumj
INTEGER :: i,j,l,m,i1,j1
! conx,conz = variables for edge of B-spline grid
! stx,stz = counters for veln grid points
! sumi,sumj = summation variables for computing b-spline

IF (.NOT.ALLOCATED(veln).OR.SIZE(veln,1).NE.nnz.OR.SIZE(veln,2).NE.nnx) THEN
        IF (ALLOCATED(veln)) DEALLOCATE(veln)
        ALLOCATE(veln(nnz,nnx))
END IF

DO i=1,nvz-1
   conz=gdz
   IF(i==nvz-1)conz=gdz+1
   DO j=1,nvx-1
      conx=gdx
      IF(j==nvx-1)conx=gdx+1
      DO l=1,conz
         stz=gdz*(i-1)+l
         DO m=1,conx
            stx=gdx*(j-1)+m
            sumi=0.0
            DO i1=1,4
               sumj=0.0
               DO j1=1,4
                  sumj=sumj+ui(m,j1)*velv(i-2+i1,j-2+j1)
               ENDDO
               sumi=sumi+vi(l,i1)*sumj
            ENDDO
            veln(stz,stx)=sumi
         ENDDO
      ENDDO
   ENDDO
ENDDO

END SUBROUTINE
