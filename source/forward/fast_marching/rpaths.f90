! This subroutine was adapted from the standalone
! program fm2dss.f90 by Nick Rawlinson.
! See FMST page for details:
! http://rses.anu.edu.au/~nick/surftomo.html

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! TYPE: SUBROUTINE
! CODE: FORTRAN 90
! This subroutine calculates ray path geometries for each
! source-receiver combination. It will also compute
! the length of each ray path.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE rpaths(csid,cscx,cscz,raypath_lengths,info)
USE globalp
USE string_conversion
IMPLICIT NONE
INTEGER, INTENT(IN) :: csid
DOUBLE PRECISION, INTENT(IN) :: cscx,cscz
DOUBLE PRECISION, DIMENSION(nrc), INTENT(OUT) :: raypath_lengths
INTEGER, INTENT(OUT) :: info
INTEGER :: i,j
INTEGER :: ipx,ipz,ipxr,ipzr,ipxo,ipzo
INTEGER :: nrp,sw
INTEGER :: isx,isz
INTEGER :: igref
DOUBLE PRECISION :: rayx,rayz
DOUBLE PRECISION :: dpl,rd1,rd2
DOUBLE PRECISION :: dtx,dtz,sred
!
! csid = current source id
! cscx,cscz = current source coordinates
! info = error flag (0=everything ok,1=error with raypath)
! ipx,ipz = coordinates of cell containing current point
! ipxr,ipzr = same as ipx,ipz except for refined grid
! ipxo,ipzo = coordinates of previous point
! nrp = number of points to describe ray
! sw = switch (0=ok,1=problem)
! isx,isz = current source cell location
! igref = ray endpoint lies in refined grid? (0=no,1=yes)
! rayx,rayz = ray path coordinates
! dpl = incremental path length of ray
! dtx,dtz = components of gradT
! sred = source to ray endpoint distance
!
info=0
!
! Locate current source cell
!
IF(asgr.EQ.1)THEN
   IF (sphere_or_plane.EQ.0) THEN
      isx=INT((cscx-goxr)/dnxr)+1
   ELSE IF (sphere_or_plane.EQ.1) THEN
      isx=INT((goxr-cscx)/dnxr)+1
   END IF
   isz=INT((cscz-gozr)/dnzr)+1
ELSE
   IF (sphere_or_plane.EQ.0) THEN
      isx=INT((cscx-gox)/dnx)+1
   ELSE IF (sphere_or_plane.EQ.1) THEN
      isx=INT((gox-cscx)/dnx)+1
   END IF
   isz=INT((cscz-goz)/dnz)+1
ENDIF
!
! Set ray incremental path length equal to half width
! of cell
!
IF (sphere_or_plane.EQ.0) THEN
  dpl=dnx*earth
  rd1=dnz*earth*SIN(gox)
ELSE IF (sphere_or_plane.EQ.1) THEN
  dpl=dnx
  rd1=dnz
END IF
IF(rd1.LT.dpl)dpl=rd1
IF (sphere_or_plane.EQ.0) THEN
  rd1=dnz*earth*SIN(gox+(nnx-1)*dnx)
ELSE IF (sphere_or_plane.EQ.1) THEN
  rd1=dnz
END IF
IF(rd1.LT.dpl)dpl=rd1
dpl=0.5*dpl
!
! Loop through all the receivers
!
raypath_lengths=0
rpaths_counter=0
DO i=1,nrc
!
!  If path does not exist, then cycle the loop
!
   IF(srs(i,csid).EQ.0)THEN
      CYCLE
   ELSEIF(srs(i,csid).EQ.1)THEN
      rpaths_counter=rpaths_counter+1
   ENDIF
!
!  The first step is to locate the receiver in the grid.
!
   IF (sphere_or_plane.EQ.0) THEN
      ipx=INT((rcx(i)-gox)/dnx)+1
   ELSE IF (sphere_or_plane.EQ.1) THEN
      ipx=INT((gox-rcx(i))/dnx)+1
   END IF
   ipz=INT((rcz(i)-goz)/dnz)+1
   sw=0
   IF(ipx.lt.1.or.ipx.ge.nnx)sw=1
   IF(ipz.lt.1.or.ipz.ge.nnz)sw=1
   IF(sw.eq.1)then
      WRITE(*,*)
      WRITE(*,*)'********************************************'
      WRITE(*,*)'ERROR in subroutine rpaths'
      WRITE(*,*)'--------------------------------------------'
      WRITE(*,*)'Receiver ',TRIM(int2str(i)),' lies outside of model:'
      IF (sphere_or_plane.EQ.0) WRITE(*,*)'    lat = ',TRIM(dble2str((pi/2-rcx(i))*180.0/pi))
      IF (sphere_or_plane.EQ.0) WRITE(*,*)'   long = ',TRIM(dble2str(rcz(i)*180.0/pi))
      IF (sphere_or_plane.EQ.1) WRITE(*,*)'    x = ',TRIM(dble2str(rcz(i)))
      IF (sphere_or_plane.EQ.1) WRITE(*,*)'    y = ',TRIM(dble2str(rcx(i)))
      WRITE(*,*)'Check input files.'
      WRITE(*,*)'TERMINATING PROGRAM!!'
      WRITE(*,*)'********************************************'
      WRITE(*,*)
      STOP
   ENDIF
   IF(ipx.eq.nnx)ipx=ipx-1
   IF(ipz.eq.nnz)ipz=ipz-1
!
!  First point of the ray path is the receiver
!
   rgx(1)=rcx(i)
   rgz(1)=rcz(i)
!
!  Test to see if receiver is in source neighbourhood
!
   IF (sphere_or_plane.EQ.0) THEN
      sred=((cscx-rgx(1))*earth)**2
      sred=sred+((cscz-rgz(1))*earth*SIN(rgx(1)))**2
   ELSE IF (sphere_or_plane.EQ.1) THEN
      sred=(cscx-rgx(1))**2
      sred=sred+(cscz-rgz(1))**2
   END IF
   sred=SQRT(sred)
   IF(sred.LT.2.0*dpl)THEN
      rgx(2)=cscx
      rgz(2)=cscz
      nrp=2
      sw=1
   ENDIF
!
!  If required, see if receiver lies within refined grid
!
   IF(asgr.EQ.1)THEN
      IF (sphere_or_plane.EQ.0) THEN
         ipxr=INT((rcx(i)-goxr)/dnxr)+1
      ELSE IF (sphere_or_plane.EQ.1) THEN
         ipxr=INT((goxr-rcx(i))/dnxr)+1
      END IF
      ipzr=INT((rcz(i)-gozr)/dnzr)+1
      igref=1
      IF(ipxr.LT.1.OR.ipxr.GE.nnxr)igref=0
      IF(ipzr.LT.1.OR.ipzr.GE.nnzr)igref=0
      IF(igref.EQ.1)THEN
         IF(nstsr(ipzr,ipxr).NE.0.OR.nstsr(ipzr+1,ipxr).NE.0)igref=0
         IF(nstsr(ipzr,ipxr+1).NE.0.OR.nstsr(ipzr+1,ipxr+1).NE.0)igref=0
      ENDIF
   ELSE
      igref=0
   ENDIF
!
!  Due to the method for calculating traveltime gradient, if the
!  the ray end point lies in the source cell, then we are also done.
!
   IF(sw.EQ.0)THEN
      IF(asgr.EQ.1)THEN
         IF(igref.EQ.1)THEN
            IF(ipxr.EQ.isx)THEN
               IF(ipzr.EQ.isz)THEN
                  rgx(2)=cscx
                  rgz(2)=cscz
                  nrp=2
                  sw=1
               ENDIF
            ENDIF
         ENDIF
      ELSE
         IF(ipx.EQ.isx)THEN
            IF(ipz.EQ.isz)THEN
               rgx(2)=cscx
               rgz(2)=cscz
               nrp=2
               sw=1
            ENDIF
         ENDIF
      ENDIF
   ENDIF
!
!  Now trace ray from receiver to "source"
!
   DO j=1,maxrp
      IF(sw.EQ.1)EXIT
!
!     Calculate traveltime gradient vector for current cell using
!     a first-order or second-order scheme.
!
      IF(igref.EQ.1)THEN
!
!        In this case, we are in the refined grid.
!
!        First order scheme applied here.
!
         dtx=ttnr(ipzr,ipxr+1)-ttnr(ipzr,ipxr)
         dtx=dtx+ttnr(ipzr+1,ipxr+1)-ttnr(ipzr+1,ipxr)
         IF (sphere_or_plane.EQ.0) THEN
            dtx=dtx/(2.0*earth*dnxr)
         ELSE IF (sphere_or_plane.EQ.1) THEN
            dtx=dtx/(2.0*dnxr)
         END IF
         dtz=ttnr(ipzr+1,ipxr)-ttnr(ipzr,ipxr)
         dtz=dtz+ttnr(ipzr+1,ipxr+1)-ttnr(ipzr,ipxr+1)
         IF (sphere_or_plane.EQ.0) THEN
            dtz=dtz/(2.0*earth*SIN(rgx(j))*dnzr)
         ELSE IF (sphere_or_plane.EQ.1) THEN
            dtz=dtz/(2.0*dnzr)
         END IF
      ELSE
!
!        Here, we are in the coarse grid.
!
!        First order scheme applied here.
!
         dtx=ttn(ipz,ipx+1)-ttn(ipz,ipx)
         dtx=dtx+ttn(ipz+1,ipx+1)-ttn(ipz+1,ipx)
         IF (sphere_or_plane.EQ.0) THEN
            dtx=dtx/(2.0*earth*dnx)
         ELSE IF (sphere_or_plane.EQ.1) THEN
            dtx=dtx/(2.0*dnx)
         END IF
         dtz=ttn(ipz+1,ipx)-ttn(ipz,ipx)
         dtz=dtz+ttn(ipz+1,ipx+1)-ttn(ipz,ipx+1)
         IF (sphere_or_plane.EQ.0) THEN
            dtz=dtz/(2.0*earth*SIN(rgx(j))*dnz)
         ELSE IF (sphere_or_plane.EQ.1) THEN
            dtz=dtz/(2.0*dnz)
         END IF
      ENDIF
!
!     Calculate the next ray path point
!
      rd1=SQRT(dtx**2+dtz**2)
      IF (sphere_or_plane.EQ.0) THEN
         rgx(j+1)=rgx(j)-dpl*dtx/(earth*rd1)
         rgz(j+1)=rgz(j)-dpl*dtz/(earth*SIN(rgx(j))*rd1)
      ELSE IF (sphere_or_plane.EQ.1) THEN
         rgx(j+1)=rgx(j)+dpl*dtx/(rd1)
         rgz(j+1)=rgz(j)-dpl*dtz/(rd1)
      END IF
!
!     Determine which cell the new ray endpoint
!     lies in.
!
      ipxo=ipx
      ipzo=ipz
      IF(asgr.EQ.1)THEN
!
!        Here, we test to see whether the ray endpoint lies
!        within a cell of the refined grid
!
         IF (sphere_or_plane.EQ.0) THEN
            ipxr=INT((rgx(j+1)-goxr)/dnxr)+1
         ELSE IF (sphere_or_plane.EQ.1) THEN
            ipxr=INT((goxr-rgx(j+1))/dnxr)+1
         END IF
         ipzr=INT((rgz(j+1)-gozr)/dnzr)+1
         igref=1
         IF(ipxr.LT.1.OR.ipxr.GE.nnxr)igref=0
         IF(ipzr.LT.1.OR.ipzr.GE.nnzr)igref=0
         IF(igref.EQ.1)THEN
            IF(nstsr(ipzr,ipxr).NE.0.OR.nstsr(ipzr+1,ipxr).NE.0)igref=0
            IF(nstsr(ipzr,ipxr+1).NE.0.OR.nstsr(ipzr+1,ipxr+1).NE.0)igref=0
         ENDIF
         IF (sphere_or_plane.EQ.0) THEN
            ipx=INT((rgx(j+1)-gox)/dnx)+1
         ELSE IF (sphere_or_plane.EQ.1) THEN
            ipx=INT((gox-rgx(j+1))/dnx)+1
         END IF
         ipz=INT((rgz(j+1)-goz)/dnz)+1
      ELSE
         IF (sphere_or_plane.EQ.0) THEN
            ipx=INT((rgx(j+1)-gox)/dnx)+1
         ELSE IF (sphere_or_plane.EQ.1) THEN
            ipx=INT((gox-rgx(j+1))/dnx)+1
         END IF
         ipz=INT((rgz(j+1)-goz)/dnz)+1
         igref=0
      ENDIF
!
!     Test the proximity of the source to the ray end point.
!     If it is less than dpl then we are done
!
      IF (sphere_or_plane.EQ.0) THEN
         sred=((cscx-rgx(j+1))*earth)**2
         sred=sred+((cscz-rgz(j+1))*earth*SIN(rgx(j+1)))**2
      ELSE IF (sphere_or_plane.EQ.1) THEN
         sred=(cscx-rgx(j+1))**2
         sred=sred+(cscz-rgz(j+1))**2
      END IF
      sred=SQRT(sred)
      sw=0
      IF(sred.LT.2.0*dpl)THEN
         rgx(j+2)=cscx
         rgz(j+2)=cscz
         nrp=j+2
         sw=1
         EXIT
      ENDIF
!
!     Due to the method for calculating traveltime gradient, if the
!     the ray end point lies in the source cell, then we are also done.
!
      IF(sw.EQ.0)THEN
         IF(asgr.EQ.1)THEN
            IF(igref.EQ.1)THEN
               IF(ipxr.EQ.isx)THEN
                  IF(ipzr.EQ.isz)THEN
                     rgx(j+2)=cscx
                     rgz(j+2)=cscz
                     nrp=j+2
                     sw=1
                     EXIT
                  ENDIF
               ENDIF
            ENDIF
         ELSE
            IF(ipx.EQ.isx)THEN
               IF(ipz.EQ.isz)THEN
                  rgx(j+2)=cscx
                  rgz(j+2)=cscz
                  nrp=j+2
                  sw=1
                  EXIT
               ENDIF
            ENDIF
         ENDIF
      ENDIF
!
!     Test whether ray path segment extends beyond
!     box boundaries
!
      IF(ipx.LT.1)THEN
         rgx(j+1)=gox
         ipx=1
         rbint=1
      ENDIF
      IF(ipx.GE.nnx)THEN
         rgx(j+1)=gox+(nnx-1)*dnx
         ipx=nnx-1
         rbint=1
      ENDIF
      IF(ipz.LT.1)THEN
         rgz(j+1)=goz
         ipz=1
         rbint=1
      ENDIF
      IF(ipz.GE.nnz)THEN
         rgz(j+1)=goz+(nnz-1)*dnz
         ipz=nnz-1
         rbint=1
      ENDIF
      IF(j.EQ.maxrp.AND.sw.EQ.0)THEN
         WRITE(*,*)'Error with ray path detected!!!'
         WRITE(*,*)'Source id: ',csid
         WRITE(*,*)'Receiver id: ',i
         info=1
         RETURN
      ENDIF
   ENDDO
!
!  Write raypath lengths to output array
!
   IF (sphere_or_plane.EQ.0) THEN
      rgx=(pi/2-rgx)*180.0/pi
      rgx=rgx*pi/180
      ! Use the Haversine formula to calculate the distance between
      ! consecutive ray points on a sphere
      ray_length=SUM((2*ASIN(SQRT((SIN((rgx(2:nrp)-rgx(1:nrp-1))/2))**2&
                            +(COS(rgx(1:nrp-1)))*(COS(rgx(2:nrp)))&
                            *(SIN((rgz(2:nrp)-rgz(1:nrp-1))/2))**2)))&
                            *180.0/pi)
      rgx=rgx*180.0/pi
      rgz=rgz*180.0/pi
   ELSE IF (sphere_or_plane.EQ.1) THEN
      ray_length=SUM(SQRT((rgx(2:nrp)-rgx(1:nrp-1))**2&
                         +(rgz(2:nrp)-rgz(1:nrp-1))**2))
   END IF
   raypath_lengths(rpaths_counter)=ray_length
!    DO j=1,nrp
!       rayx=rgx(j)
!       rayz=rgz(j)
!       WRITE(*,*)rayz,rayx
!    ENDDO
ENDDO

END SUBROUTINE rpaths