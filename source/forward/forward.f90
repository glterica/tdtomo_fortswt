! ********************************************************************* !
! Forward modelling module (part of TSWT package)
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


MODULE forward

USE input_files, ONLY: modelling_method,forward_file
USE acquisition, ONLY: number_of_measurements
USE string_conversion

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE forward_modelling(number_of_cells,voronoi_model,modelled_data,info,data_for_noise)

IMPLICIT NONE
INTEGER, INTENT(IN)                                                             :: number_of_cells
DOUBLE PRECISION, DIMENSION(:,:), INTENT(IN)                                    :: voronoi_model
DOUBLE PRECISION, DIMENSION(number_of_measurements), INTENT(OUT)                :: modelled_data
INTEGER, INTENT(OUT)                                                            :: info
DOUBLE PRECISION, DIMENSION(number_of_measurements), INTENT(OUT), OPTIONAL      :: data_for_noise

IF (modelling_method.EQ.0.OR.modelling_method.EQ.1) THEN
        
        IF (.NOT.PRESENT(data_for_noise)) THEN
                CALL forward_with_fmmin2d(number_of_cells,voronoi_model(1:number_of_cells,[1,2,4]),&
                                          modelled_data,info)
        ELSE IF (PRESENT(data_for_noise)) THEN
                CALL forward_with_fmmin2d(number_of_cells,voronoi_model(1:number_of_cells,[1,2,4]),&
                                          modelled_data,info,data_for_noise)
        END IF
        
ELSE
        
        WRITE(*,*)
        WRITE(*,*)'********************************************'
        WRITE(*,*)'ERROR in subroutine forward_modelling'
        WRITE(*,*)'--------------------------------------------'
        WRITE(*,*)'Modelling method ',TRIM(int2str(modelling_method)),' does not exist!'
        WRITE(*,*)'Check input file mksamples.in'
        WRITE(*,*)'TERMINATING PROGRAM!!'
        WRITE(*,*)'********************************************'
        WRITE(*,*)
        STOP
        
END IF

END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE forward_with_fmmin2d(number_of_cells,voronoi_model,traveltime_data,info,raypath_data)

USE fast_marching
USE voronoi_operations
USE chains, ONLY: x_min,x_max,y_min,y_max,noise_type
IMPLICIT NONE
INTEGER, INTENT(IN)                                     :: number_of_cells
DOUBLE PRECISION, DIMENSION(:,:), INTENT(IN)            :: voronoi_model
DOUBLE PRECISION, DIMENSION(:), INTENT(OUT)             :: traveltime_data
INTEGER, INTENT(OUT)                                    :: info
DOUBLE PRECISION, DIMENSION(:), INTENT(OUT), OPTIONAL   :: raypath_data
INTERFACE
        SUBROUTINE create_velocity_grid2D(ncell,voronoi_model)
                INTEGER, INTENT(IN)                                     :: ncell
                DOUBLE PRECISION, DIMENSION(ncell,3), INTENT(IN)        :: voronoi_model
        END SUBROUTINE
END INTERFACE

IF (.NOT.ALLOCATED(velv)) THEN
        
        ! Define modelling method
        sphere_or_plane=modelling_method
        
        ! Define input file name for forward modelling and model boundaries
        grid2D_file=forward_file
        north=y_max
        south=y_min
        west=x_min
        east=x_max
        
        ! Initialise the modelling grid and various modelling parameters
        CALL initialize_fm()
        
        ! Define whether raypaths should be computed
        IF (noise_type.EQ.2) THEN
                calculate_paths=1
        ELSE
                IF (noise_type.EQ.1) CALL source_receiver_distances()
                calculate_paths=0
        END IF
        
END IF

! Create 2D velocity model
CALL create_velocity_grid2D(number_of_cells,voronoi_model)

! Run forward modelling
IF (calculate_paths.EQ.0) THEN
        CALL run_fmmin2d(traveltime_data,info)
ELSE IF (calculate_paths.EQ.1) THEN
        CALL run_fmmin2d(traveltime_data,info,raypath_data)
        IF (sphere_or_plane.EQ.1) raypath_data=raypath_data/100
END IF

END SUBROUTINE

END MODULE