! ********************************************************************* !
! Program to model a seismic surface-wave traveltime dataset
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


PROGRAM forward_model

USE acquisition
USE fast_marching
USE string_conversion
IMPLICIT NONE
INTEGER :: nn,info,i,j,ss,rr
CHARACTER(len=30) :: traveltime_file,raylength_file,grid_file
DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE     :: traveltimes,raypath_lengths
LOGICAL :: verbose=.FALSE.

! Read input file
OPEN(UNIT=10,FILE='forward_model.in',STATUS='OLD')
READ(10,*)
READ(10,*)
READ(10,*)
READ(10,1)sources_file
READ(10,1)receivers_file
READ(10,1)data_file
READ(10,*)sphere_or_plane
READ(10,1)grid_file
READ(10,*)gdx
READ(10,*)gdz
READ(10,*)asgr
READ(10,*)sgdl
READ(10,*)sgs
READ(10,*)fom
READ(10,*)snb
READ(10,*)earth
READ(10,*)
READ(10,*)
READ(10,*)
READ(10,1)traveltime_file
READ(10,1)raylength_file
1   FORMAT(a30)
CLOSE(10)

 calculate_paths=1

CALL read_data(1)

IF (verbose) THEN
        
        WRITE(*,*)'Number of sources: ',TRIM(int2str(number_of_sources))
        DO nn=1,number_of_sources
                WRITE(*,*)sources(nn,:)
        END DO
        WRITE(*,*)
        
        WRITE(*,*)'Number of receivers: ',TRIM(int2str(number_of_receivers))
        DO nn=1,number_of_receivers
                WRITE(*,*)receivers(nn,:)
        END DO
        WRITE(*,*)
        
        WRITE(*,*)'Number of measurements: ',TRIM(int2str(number_of_measurements))
        DO nn=1,number_of_measurements
                WRITE(*,*)nn,source_receiver_pairs(nn,:)
        END DO
        WRITE(*,*)
        
END IF

ALLOCATE(traveltimes(number_of_measurements),raypath_lengths(number_of_measurements))

OPEN(UNIT=20,FILE=grid_file,STATUS='OLD')
READ(20,*)nvx,nvz
READ(20,*)goxd,gozd
READ(20,*)dvxd,dvzd

CALL allocate_arrays()
CALL initialize_gridder()

DO i=0,nvz+1
        DO j=0,nvx+1
                READ(20,*)velv(j,i)
        END DO
END DO

CLOSE(20)

CALL run_fmmin2d(traveltimes,info,raypath_lengths)

IF (verbose) WRITE(*,*)'Problem flag: ',TRIM(int2str(info))
IF (verbose) WRITE(*,*)

IF (verbose) WRITE(*,*)'Traveltimes and raypath lengths:'
OPEN(UNIT=30,FILE=traveltime_file,STATUS='REPLACE')
OPEN(UNIT=40,FILE=raylength_file,STATUS='REPLACE')
nn=0
DO ss=1,number_of_sources
        DO rr=1,number_of_receivers
                IF (srs(rr,ss).EQ.1) THEN
                        nn=nn+1
                        IF (verbose) WRITE(*,*)nn,traveltimes(nn),raypath_lengths(nn)
                        WRITE(30,*)INT(1),traveltimes(nn)
                        WRITE(40,*)INT(1),raypath_lengths(nn)
                ELSE
                        WRITE(30,*)INT(0),INT(0)
                        WRITE(40,*)INT(0),INT(0)
                END IF
        END DO
END DO
CLOSE(30)
CLOSE(40)

END PROGRAM